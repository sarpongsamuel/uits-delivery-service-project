﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class PagesController : Controller
    {
         
        private PagesService _pagesService;
        private CustomerOrderService _customerOrderService;
        private CustomerService _customerService;
        public PagesController(PagesService pagesService, CustomerOrderService customerOrderService, CustomerService customerService)
        {
            _pagesService = pagesService;
            _customerOrderService = customerOrderService;
            _customerService = customerService;
        }
        public IActionResult Index()
        {
            var locations = _pagesService.ListLocation();

            ViewData["locations"] = locations;

            var prices = _pagesService.ListPrices();
            ViewData["Prices"] = prices;

            var model = _pagesService.create();
            return View(model);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Services()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Request()
        {
            var model = _pagesService.create();
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(CustomerViewModel model)
         {
            
                var exist = _customerService.Login(model);

                if ( exist)
                {
                    return RedirectToAction("Request", "PagesController");
                }
                else
                {
                    ViewBag.Message = "Invalid Credentials Entered";
                    return View();
                }
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}