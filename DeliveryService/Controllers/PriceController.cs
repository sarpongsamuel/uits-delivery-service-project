﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class PriceController : Controller
    {
        // GET: Price
        private PriceService _priceService;

        public PriceController(PriceService priceService)
        {
            _priceService = priceService;
        }
        public ActionResult Index()
        {
            var model = _priceService.GetPrices();
            return View(model);
            
        }

        // GET: Price/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var model = _priceService.GetPriceDetails(id);
                return View(model);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Price/Create
        public ActionResult Create()
        {
            var model = _priceService.CreatePrice();
            return View(model);
        }

        // POST: Price/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PriceViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var result = _priceService.AddPrice(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new Exception();


            }
            catch
            {
                ModelState.AddModelError(string.Empty, "oops something went wrong");
                return View();
            }
        }

        // GET: Price/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _priceService.GetPriceDetails(id);
                return View(model);
            }
            catch
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Price/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PriceViewModel model)
        {
            try
            {
                bool result = _priceService.UpdatePrice(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new Exception();
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Ooops! Something went wrong!");
                return View();
            }
        }

        // GET: Price/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _priceService.GetPriceDetails(id);
                return View(model);
            }
            catch
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Price/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, PriceViewModel model)
        {
            try
            {
                bool result = _priceService.RemovePrice(id);

                if (result)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new Exception();
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Ooops! Something went wrong!");
                return View();
            }
        }
    }
}