﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class CustomerController : Controller
    {


        private CustomerService _customerService;

        public CustomerController(CustomerService customerService)
        {
            _customerService = customerService;
        }

        // GET: Customer
        public ActionResult Index()
        {
            var model = _customerService.GetCustomers(); ;
            return View(model);

        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var model = _customerService.GetCustomerDetails(id);
                return View(model);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerViewModel model)
        {
            // TODO: Add insert logic here
            try
            {
                
                var result = _customerService.AddCustomer(model);

                if (result)
                {
                    
                    return RedirectToAction(nameof(Index));
                }
                
                throw new Exception();
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "oops something went wrong");
                return View();
            }

        }



        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _customerService.GetCustomerDetails(id);
                return View(model);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }


        // POST: Customer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CustomerViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                var result = _customerService.UpdateCustomer(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _customerService.GetCustomerDetails(id);
                return View(model);

            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Customer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, CompanyViewModel model)
        {
            try
            {

                // TODO: Add delete logic here
                bool result = _customerService.RemoveCustomer(id);
                if (result)
                {
                    return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }
    }

}
