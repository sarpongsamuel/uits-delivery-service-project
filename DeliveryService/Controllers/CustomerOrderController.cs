﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class CustomerOrderController : Controller
    {
        private CustomerOrderService _customerOrderService;
        public CustomerOrderController(CustomerOrderService customerOrderService)
        {
            _customerOrderService = customerOrderService;
        }

        // GET: CustomerOrder
        public ActionResult Index()
        {
            var model = _customerOrderService.GetCustomerOrder();
            return View(model);
        }

        // GET: CustomerOrder/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CustomerOrder/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CustomerOrder/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CustomerOrder/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CustomerOrder/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CustomerOrder/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CustomerOrder/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}