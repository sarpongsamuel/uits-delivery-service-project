﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class LocationController : Controller
    {
        private LocationService _locationService;
        public LocationController(LocationService locationService)
        {
            _locationService = locationService;
        }
        // GET: Location
        public ActionResult Index()
        {
            var model = _locationService.GetLocation();
            return View(model);
        }

        // GET: Location/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var model = _locationService.GetLocationDetails(id);
                return View(model);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Location/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Location/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LocationViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var result = _locationService.AddLocation(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new Exception();


            }
            catch
            {
                ModelState.AddModelError(string.Empty, "oops something went wrong");
                return View();
            }
        }

        // GET: Location/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _locationService.GetLocationDetails(id);
                return View(model);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Location/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, LocationViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                var result = _locationService.UpdateLocation(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }

        // GET: Location/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _locationService.GetLocationDetails(id);
                return View(model);

            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Location/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, LocationViewModel model)
        {
            try
            {

                // TODO: Add delete logic here
                bool result = _locationService.RemoveLocation(id);
                if (result)
                {
                    return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }
    }
}