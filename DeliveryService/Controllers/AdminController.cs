﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class AdminController : Controller
    {
        private AdminService _adminService;

        public AdminController(AdminService adminService)
        {
            _adminService = adminService;
        }

        public IActionResult Index()
        {
            var countCustomer = _adminService.CountCustomer();
            var countLocation = _adminService.CountLocation();
            var countCompany = _adminService.CountCompany();

            ViewData["company"] = countCompany;
            ViewData["location"] = countLocation;
            ViewData["customer"] = countCustomer;

            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(AdminViewModel model)
        {

            var exist = _adminService.Login(model);

            if (exist)
            {
                return RedirectToAction("Index", "AdminController");
            }
            else
            {
                ViewBag.Message = "Invalid Credentials Entered";
                return View();
            }
        }


    }
}