﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryService.Models.Services;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryService.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company

        private CompanyService _companyService;

        public CompanyController(CompanyService companyService)
        {
            _companyService = companyService;
        }
        public ActionResult Index()
        {
            var model = _companyService.GetCompanies();
            return View(model);
        }

        // GET: Company/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var model = _companyService.GetCompanyDetails(id);
                return View(model);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }




        // GET: Company/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Company/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompanyViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var result = _companyService.AddCompany(model);

                if (result)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new Exception();

                
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "oops something went wrong");
                return View();
            }
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _companyService.GetCompanyDetails(id);
                return View(model);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
           
        }

        // POST: Company/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CompanyViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                var result = _companyService.UpdateCompany(model);

                if (result)
                {
                return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _companyService.GetCompanyDetails(id);
                return View(model);
                
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        // POST: Company/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, CompanyViewModel model)
        {
            try
            {
                
                // TODO: Add delete logic here
                bool result = _companyService.RemoveCompany(id);
                if (result)
                {
                return RedirectToAction(nameof(Index));

                }
                throw new Exception();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "OOps something went wrong");
                return View();
            }
        }
    }
}