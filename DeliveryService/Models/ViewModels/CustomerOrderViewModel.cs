﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.ViewModels
{
    public class CustomerOrderViewModel
    {
        public int Id { get; set; }
        [Required]
        public string ShopReceipt { get; set; }
        [NotMapped]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        [NotMapped]
        public int PickupPointID { get; set; }
        public string PickupPoint { get; set; }

        [NotMapped]
        public int DeliveryPointId { get; set; }
        public string DeliveryPoint { get; set; }

        [NotMapped]
        public int CompanyId { get; set; }
        public string Company { get; set; }

        [NotMapped]
        public int PriceId { get; set; }
        public decimal Price { get; set; }


        //a dropdown select for pickup and destination
        [NotMapped]
        public SelectList DeliveryPointList { get; set; }
        [NotMapped]
        public SelectList PickupPointList { get; set; }
        [NotMapped]
        public SelectList CompanyList { get; set; }



    }
}
