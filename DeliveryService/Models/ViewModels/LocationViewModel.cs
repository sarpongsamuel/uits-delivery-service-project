﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.ViewModels
{
    public class LocationViewModel
    {
        public int Id { get; set; }
        [DisplayName("Location")]
        [Required]

        public string LocationName { get; set; }

    }
}
