﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.ViewModels
{
    public class PriceViewModel
    {
        public int Id { get; set; }
        [Required]
        public decimal Amount { get; set; }

        //company pointing to 
        [NotMapped]
        [Required]
        public int CompanyId { get; set; }
        public string Company { get; set; }

        //setting up pickup point for price
        [NotMapped]
        [Required]
        public int PickupPointID { get; set; }
        public string PickupPoint { get; set; }

        //setting up destination price for price
        [NotMapped]
        [Required]
        public int DeliveryPointID { get; set; }
        public string DeliveryPoint { get; set; }


        //a dropdown select for pickup and destination
        [NotMapped]
        
        public SelectList DeliveryPointList { get; set; }
        [NotMapped]
        public SelectList PickupPointList { get; set; }
        [NotMapped]
        public SelectList CompanyList { get; set; }

    }
}
