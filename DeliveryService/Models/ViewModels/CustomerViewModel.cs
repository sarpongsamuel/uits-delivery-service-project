﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.ViewModels
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        [DisplayName ("Customer Name")]
        [Required(ErrorMessage ="Customer Name required")]
        public string CustomerName { get; set; }

        [DisplayName("Customer Phone")]
        [Required(ErrorMessage = "Customer Phone required")]
        public string CustomerContact { get; set; }
    }
}
