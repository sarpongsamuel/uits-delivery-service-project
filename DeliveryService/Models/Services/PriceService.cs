﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class PriceService
    {
        //-----------------------setting up private data members for DB and services ---------------------------
        private DeliveryServiceContext _context;
        private CompanyService _companyService;
        private LocationService _locationService;

        // ---------------------calling a constructor to ensure data mambers are  assigned before preceeding----------- 
        public PriceService(DeliveryServiceContext context, CompanyService companyService, LocationService locationService)
        {
            _context = context;
            _companyService = companyService;
            _locationService = locationService;
        }

        //------------------------------------Add company and loction to create View --------------------------------------------------


        public PriceViewModel CreatePrice()
        {
            PriceViewModel model = new PriceViewModel();

            List<CompanyViewModel> company = _companyService.GetCompanies();
            List<LocationViewModel> location = _locationService.GetLocation();

            SelectList companyList = new SelectList(company, "Id", "CompanyName");
            SelectList deliveryPointList = new SelectList(location, "Id", "LocationName");
            SelectList pickupPointList = new SelectList(location, "Id", "LocationName");


            model.CompanyList = companyList;
            model.DeliveryPointList = deliveryPointList;
            model.PickupPointList = pickupPointList;
            

            return model;
        }


        //--------------------------------add new price and save to database ------------------------------
        public bool AddPrice(PriceViewModel model)
        {
            try
            {
                Price price = new Price();
                price.Amount = model.Amount;
                price.CompanyId = model.CompanyId;
                price.DeliveryPoint = model.DeliveryPointID;
                price.PickupPoint = model.PickupPointID;

                _context.Price.Add(price);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }


        //------------------------------------------------- List All Prices ----------------------------
        public List<PriceViewModel> GetPrices()
        {
            try
            {
                List<Price> price = _context.Price.Include(x => x.Company)
                                                  .Include(x => x.DeliveryPointNavigation)
                                                  .Include(x => x.PickupPointNavigation)
                                                  .ToList();

                List<PriceViewModel> viewModel = price.Select(x => new PriceViewModel
                {
                    Id = x.PriceId,
                    //fetching related company
                    Company = x.Company.CompanyName,
                    Amount = x.Amount,
                    //fetching pickup point name
                    PickupPoint = x.PickupPointNavigation.LocationName,
                    //fetching delivery point name
                    DeliveryPoint = x.DeliveryPointNavigation.LocationName,
                    
                }).ToList();

                return viewModel;
            }

            catch (Exception)
            {
                throw;
            }
        }


        //--------------------------------------------- Get Price details ------------------------------------
        public PriceViewModel GetPriceDetails(int id)
        {
            try
            {
                Price price = _context.Price.Where(x => x.PriceId == id)
                                                    .Include(x => x.DeliveryPointNavigation)
                                                    .Include(x => x.PickupPointNavigation)
                                                    .Include(x => x.Company)
                                                    .FirstOrDefault();
                PriceViewModel viewModel = new PriceViewModel
                {
                    Id = price.PriceId,
                    PickupPoint = price.PickupPointNavigation.LocationName,
                    DeliveryPoint = price.DeliveryPointNavigation.LocationName,
                    Amount = price.Amount,
                    Company = price.Company.CompanyName

                };

                return viewModel;

            }
            catch (Exception)
            {
                throw;
            }
        }

        
        //-------------------------------Update Price Details ---------------------------------------

        public bool UpdatePrice(PriceViewModel model)
        {
            try
            {
                Price price = _context.Price.Where(x => x.PriceId == model.Id).FirstOrDefault();
                price.Amount = model.Amount;
                price.CompanyId = model.CompanyId;
                price.DeliveryPoint = model.DeliveryPointID;
                price.PickupPoint = model.PickupPointID;

                _context.Price.Update(price);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //----------------------------------------delete Price from database -------------------------

        public bool RemovePrice(int id)
        {
            try
            {
                Price price = _context.Price.Where(x => x.PriceId == id).First();
                _context.Price.Remove(price);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}










