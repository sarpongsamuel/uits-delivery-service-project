﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class CompanyService
    {
        private DeliveryServiceContext _context;
        public CompanyService(DeliveryServiceContext context)
        {
            _context = context;
        }

        //add new company to database

        public bool AddCompany(CompanyViewModel model)
        {
            try
            {
                Company company = new Company();
                company.CompanyName = model.CompanyName;
                company.CompanyContact = model.CompanyContact;

                _context.Company.Add(company);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }


        //list all companies in the database
        public List<CompanyViewModel> GetCompanies()
        {
            try
            {
                var companies = _context.Company.ToList();
                List<CompanyViewModel> model = companies.Select(x => new CompanyViewModel
                {
                    Id = x.CompanyId,
                    CompanyName = x.CompanyName,
                    CompanyContact = x.CompanyContact

                }).ToList();

                return model;
            }
            catch (Exception)
            {
                List<CompanyViewModel> emptyModel = new List<CompanyViewModel>();
                return emptyModel;
            }
        }

        //show company details 

        public CompanyViewModel  GetCompanyDetails(int id)
        {
            try
            {
                Company company = _context.Company.Where(x => x.CompanyId == id).First();
                CompanyViewModel model = new CompanyViewModel
                {
                    Id = company.CompanyId,
                    CompanyName = company.CompanyName,
                    CompanyContact = company.CompanyContact
                };

                return model;
            }
            catch (Exception)
            {

                CompanyViewModel emptyModel = new CompanyViewModel();
                return emptyModel;
            }
        }


        //update company details
        public bool UpdateCompany(CompanyViewModel model)
        {
            try
            {
                Company company = _context.Company.Where(x => x.CompanyId == model.Id).First();
                company.CompanyName = model.CompanyName;
                company.CompanyContact = model.CompanyContact;

                _context.Company.Update(company);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
            
        }


        //delete company from database
        public bool RemoveCompany(int id)
        {
            try
            {
                Company company = _context.Company.Where(x => x.CompanyId == id).First();
                _context.Company.Remove(company);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
            
        }


















    }
}
