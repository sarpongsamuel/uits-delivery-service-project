﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class AdminService
    {
        private DeliveryServiceContext _context;

        public AdminService(DeliveryServiceContext context)
        {
            _context = context;
        }

        //get customers count
        public int CountCustomer()
        {
            var countCustomer = _context.Customer.Count();
            return countCustomer;
        }

        public int CountLocation()
        {
            var countLocation = _context.Location.Count();
            return countLocation;
        }

        public int CountCompany()
        {
            var countCompany = _context.Company.Count();
            return countCompany;
        }

        public bool Login(AdminViewModel model)
        {
            try
            {
                bool exist = _context.Admin.Any(x =>x.Email == model.Email && x.Password == model.Password);

                if (exist)
                {
                    return true;
                }
                else
                {
                    return false;

                }

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
