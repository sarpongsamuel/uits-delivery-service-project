﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class LocationService
    {
        private DeliveryServiceContext _context;

        public LocationService(DeliveryServiceContext context)
        {
            _context = context;
        }

        public Location Location { get; private set; }

        //----------------------------------- adding a new location to the database---------------------------------
        public bool AddLocation(LocationViewModel model)
        {

            try
            {
                Location location = new Location();
                location.LocationName = model.LocationName;

                _context.Location.Add(location);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        //----------------------------------------- getting all list of Location ------------------------------------
        public List<LocationViewModel> GetLocation()

        {
            try
            {
                //returns all location in a list form
                var location = _context.Location.ToList();

                List<LocationViewModel> model = location.Select(x => new LocationViewModel
                {
                    Id = x.LocationId,
                    LocationName = x.LocationName,

                }).ToList();

                return model;
            }
            catch (Exception)
            {

                List<LocationViewModel> emptyModel = new List<LocationViewModel>();
                return emptyModel;
            }




        }
        // --------------------------------------getting Location details-------------------------------------- 
        public LocationViewModel GetLocationDetails(int id)
        {
            try
            {
                //getting the first Location with the id 
                Location location = _context.Location.Where(x => x.LocationId == id).First();
                //locating the details of the Location in the Locationviewmodel
                LocationViewModel model = new LocationViewModel
                {
                    Id = location.LocationId,
                    LocationName = location.LocationName
                   
                };
                return model;
            }
            catch (Exception)
            {

                LocationViewModel emptyModel = new LocationViewModel();
                return emptyModel;


            }
        }
        //-------------------------------------------updating a Location-------------------------
        public bool UpdateLocation(LocationViewModel model)
        {
            try
            {
                Location location= _context.Location.Where(x => x.LocationId == model.Id).First();
                location.LocationName = model.LocationName;
         

                _context.Location.Update(location);
                _context.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }

        }
        //------------------------------------- removing a Location by id ----------------------------------- 
        public bool RemoveLocation(int id)
        {
            try
            {
                Location location = _context.Location.Where(x => x.LocationId == id).First();

                _context.Location.Remove(location);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

    }
}
