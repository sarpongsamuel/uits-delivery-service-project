﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class PagesService
    {
        private DeliveryServiceContext _context;
        private PriceService _priceService;
        private LocationService _locationService;
        private CustomerOrderService _customerOrderService;
        private CustomerService _customerService;


        public PagesService(
            DeliveryServiceContext context,
            PriceService priceService, 
            LocationService locationService, 
            CustomerOrderService customerOrderService,
            CustomerService customerService

            )
        {
            _context = context;
            _locationService = locationService;
            _priceService = priceService;
            _customerOrderService = customerOrderService;
        }

        public List<LocationViewModel> ListLocation()
        {

        var locations = _locationService.GetLocation();

        return  locations;

        }

       public List<PriceViewModel> ListPrices()
        {
            var Prices = _priceService.GetPrices();
            return Prices;
        }

        public CustomerOrderViewModel create()
        {
            var model = _customerOrderService.CreateOrder();
            return model;
        }

       
    }
}
