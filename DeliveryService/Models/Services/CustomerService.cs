﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class CustomerService
    {
        private DeliveryServiceContext _context;

        public CustomerService(DeliveryServiceContext context)
        {
            _context = context;
        }

        //add new customer
        public bool AddCustomer(CustomerViewModel model)
        {
            try
            {
                Customer customer = new Customer();
                bool UserExit = _context.Customer.Any(x => x.CustomerContact == model.CustomerContact);

                if (UserExit)
                {
                    return true;
                }
                customer.CustomerName = model.CustomerName;
                customer.CustomerContact = model.CustomerContact;

                //add new customer and save to database
                _context.Customer.Add(customer);
                _context.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        //login service
        public bool Login(CustomerViewModel model)
        {
            try
            {
                
                bool exist = _context.Customer.Any(x => x.CustomerContact == model.CustomerContact);


                if (exist)
                {
                    return true;
                }
                else
                {
                    return false;

                }
                  
            }
            catch (Exception)
            {
                return false;
            }
        }


        //list all customer in the database

        public List<CustomerViewModel> GetCustomers()
        {
            try
            {
                var customers = _context.Customer.ToList();
                List<CustomerViewModel> model = customers.Select(x => new CustomerViewModel
                {
                    Id = x.CustomerId,
                    CustomerName = x.CustomerName,
                    CustomerContact = x.CustomerContact
                }).ToList();

                return model;
            }
            catch (Exception)
            {

                List<CustomerViewModel> emptyModel = new List<CustomerViewModel>();
                return emptyModel;
            }
        }

        // get customer details 
        public CustomerViewModel GetCustomerDetails(int id)
        {
            try
            {
                Customer customer = _context.Customer.Where(x => x.CustomerId == id).First();
                CustomerViewModel model = new CustomerViewModel
                {
                    Id = customer.CustomerId,
                    CustomerName = customer.CustomerName,
                    CustomerContact = customer.CustomerContact
                };

                return model;

            }
            catch (Exception)
            {
                CustomerViewModel emptyModel = new CustomerViewModel();
                return emptyModel;
            }
           
        }

        //update customer details
        public bool UpdateCustomer(CustomerViewModel model)
        {
            try
            {
                Customer customer = _context.Customer.Where(x => x.CustomerId == model.Id).First();
                customer.CustomerName = model.CustomerName;
                customer.CustomerContact = model.CustomerContact;

                //update and save changes to database
                _context.Customer.Update(customer);
                _context.SaveChanges();
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        //delete customer details from database

        public bool RemoveCustomer(int id)
        {
            try
            {
                Customer customer = _context.Customer.Where(x => x.CustomerId == id).First();
                _context.Customer.Remove(customer);
                _context.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
























    }
}
