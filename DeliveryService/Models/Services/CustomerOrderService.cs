﻿using DeliveryService.Models.Data.DeliveryServiceContext;
using DeliveryService.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryService.Models.Services
{
    public class CustomerOrderService
    {
        private DeliveryServiceContext _context;
        private CompanyService _companyService;
        private LocationService _locationService;
        private PriceService _priceService;


        public CustomerOrderService(DeliveryServiceContext context, CompanyService companyService, LocationService locationService, PriceService priceService)
        {
            _context = context;
            _companyService = companyService;
            _priceService = priceService;
            _locationService = locationService;
        }

        // 
        public CustomerOrderViewModel CreateOrder()
        {
            CustomerOrderViewModel model = new CustomerOrderViewModel();

            List<CompanyViewModel> company = _companyService.GetCompanies();
            List<LocationViewModel> location = _locationService.GetLocation();
            List<PriceViewModel> price = _priceService.GetPrices();

            SelectList companyList = new SelectList(company, "Id", "CompanyName");
            SelectList deliveryPointList = new SelectList(location, "Id", "LocationName");
            SelectList pickupPointList = new SelectList(location, "Id", "LocationName");
            SelectList PriceList = new SelectList(price, "Id", "Amount");


            model.CompanyList = companyList;
            model.DeliveryPointList = deliveryPointList;
            model.PickupPointList = pickupPointList;


            return model;
        }


        // ---------------------------------- add new order to database ----------------------------

        public bool AddOrder(CustomerOrderViewModel model)
        {
            try
            {
                CustomerOrder customerOrder = new CustomerOrder();
                customerOrder.ShopReceipt = model.ShopReceipt;
                customerOrder.CustomerId = model.CustomerId;
                customerOrder.DeliveryPoint = model.DeliveryPointId;
                customerOrder.PickUpPoint = model.PickupPointID;
                customerOrder.PriceId = model.PriceId;

                _context.CustomerOrder.Add(customerOrder);
                _context.SaveChanges();

                return true;

            }
            catch (Exception)
            {

                return false;
            }
            
        }

        //delete customer order
        public bool DeleteCustomerOrder(int id)
        {
            try
            {
                CustomerOrder customerOrder = _context.CustomerOrder.Where(x => x.CustomerId == id).FirstOrDefault();

                _context.CustomerOrder.Remove(customerOrder);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //list customer order on admin panel
        public List<CustomerOrderViewModel> GetCustomerOrder()
        {
            try
            {
                List<CustomerOrder> customerOrder = _context.CustomerOrder
                                                .Include(x => x.Customer)
                                                .Include(x => x.DeliveryPoint)
                                                .Include(x => x.Price)
                                                .Include(x => x.CompanyId)
                                                .ToList();

                List<CustomerOrderViewModel> viewModel = customerOrder.Select(x =>
                {
                    return new CustomerOrderViewModel
                    {
                        Id = x.CustomerId,
                        ShopReceipt = x.ShopReceipt,
                        PickupPoint = x.PickUpPointNavigation.LocationName,
                        DeliveryPoint = x.DeliveryPointNavigation.LocationName,
                        CustomerName = x.Customer.CustomerName,
                        Price = x.Price.Amount,
                        CompanyId = x.CompanyId
                    };
                }).ToList();

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
