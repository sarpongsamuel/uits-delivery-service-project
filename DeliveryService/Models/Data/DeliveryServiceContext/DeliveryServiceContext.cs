﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DeliveryService.Models.ViewModels;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class DeliveryServiceContext : DbContext
    {
        public DeliveryServiceContext()
        {
        }

        public DeliveryServiceContext(DbContextOptions<DeliveryServiceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerOrder> CustomerOrder { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Price> Price { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=DeliveryService; Integrated Security = true; MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.AdminId)
                    .HasColumnName("AdminID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasKey(e => e.CompanyId)
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CompanyContact)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerContact)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CustomerOrder>(entity =>
            {
                entity.HasKey(e => new { e.CustomerOrderId, e.CustomerId })
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.CustomerOrderId)
                    .HasColumnName("CustomerOrderID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.PriceId).HasColumnName("PriceID");

                entity.Property(e => e.ShopReceipt)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerOrder)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CustomerCustomerOrder");

                entity.HasOne(d => d.DeliveryPointNavigation)
                    .WithMany(p => p.CustomerOrderDeliveryPointNavigation)
                    .HasForeignKey(d => d.DeliveryPoint)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LocationCustomerOrd3");

                entity.HasOne(d => d.PickUpPointNavigation)
                    .WithMany(p => p.CustomerOrderPickUpPointNavigation)
                    .HasForeignKey(d => d.PickUpPoint)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LocationCustomerOrder");

                entity.HasOne(d => d.Price)
                    .WithMany(p => p.CustomerOrder)
                    .HasForeignKey(d => new { d.PriceId, d.CompanyId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("PriceCustomerOrder");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasKey(e => e.LocationId)
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.LocationName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Price>(entity =>
            {
                entity.HasKey(e => new { e.PriceId, e.CompanyId })
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.PriceId)
                    .HasColumnName("PriceID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10, 0)");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Price)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CompanyPrice");

                entity.HasOne(d => d.DeliveryPointNavigation)
                    .WithMany(p => p.PriceDeliveryPointNavigation)
                    .HasForeignKey(d => d.DeliveryPoint)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LocationPri5");

                entity.HasOne(d => d.PickupPointNavigation)
                    .WithMany(p => p.PricePickupPointNavigation)
                    .HasForeignKey(d => d.PickupPoint)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LocationPrice");
            });
        }

        public DbSet<DeliveryService.Models.ViewModels.PriceViewModel> PriceViewModel { get; set; }

        public DbSet<DeliveryService.Models.ViewModels.CustomerOrderViewModel> CustomerOrderViewModel { get; set; }

        public DbSet<DeliveryService.Models.ViewModels.LoginViewModel> LoginViewModel { get; set; }

        public DbSet<DeliveryService.Models.ViewModels.AdminViewModel> AdminViewModel { get; set; }
    }
}
