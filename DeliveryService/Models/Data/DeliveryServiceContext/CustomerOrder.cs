﻿using System;
using System.Collections.Generic;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class CustomerOrder
    {
        public string CustomerOrderId { get; set; }
        public int CustomerId { get; set; }
        public string ShopReceipt { get; set; }
        public int PickUpPoint { get; set; }
        public int DeliveryPoint { get; set; }
        public int PriceId { get; set; }
        public int CompanyId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Location DeliveryPointNavigation { get; set; }
        public virtual Location PickUpPointNavigation { get; set; }
        public virtual Price Price { get; set; }
    }
}
