﻿using System;
using System.Collections.Generic;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class Location
    {
        public Location()
        {
            CustomerOrderDeliveryPointNavigation = new HashSet<CustomerOrder>();
            CustomerOrderPickUpPointNavigation = new HashSet<CustomerOrder>();
            PriceDeliveryPointNavigation = new HashSet<Price>();
            PricePickupPointNavigation = new HashSet<Price>();
        }

        public int LocationId { get; set; }
        public string LocationName { get; set; }

        public virtual ICollection<CustomerOrder> CustomerOrderDeliveryPointNavigation { get; set; }
        public virtual ICollection<CustomerOrder> CustomerOrderPickUpPointNavigation { get; set; }
        public virtual ICollection<Price> PriceDeliveryPointNavigation { get; set; }
        public virtual ICollection<Price> PricePickupPointNavigation { get; set; }
    }
}
