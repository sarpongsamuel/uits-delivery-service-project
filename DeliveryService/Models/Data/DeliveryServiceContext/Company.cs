﻿using System;
using System.Collections.Generic;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class Company
    {
        public Company()
        {
            Price = new HashSet<Price>();
        }

        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyContact { get; set; }

        public virtual ICollection<Price> Price { get; set; }
    }
}
