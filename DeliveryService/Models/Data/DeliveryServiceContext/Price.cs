﻿using System;
using System.Collections.Generic;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class Price
    {
        public Price()
        {
            CustomerOrder = new HashSet<CustomerOrder>();
        }

        public int PriceId { get; set; }
        public int CompanyId { get; set; }
        public decimal Amount { get; set; }
        public int PickupPoint { get; set; }
        public int DeliveryPoint { get; set; }

        public virtual Company Company { get; set; }
        public virtual Location DeliveryPointNavigation { get; set; }
        public virtual Location PickupPointNavigation { get; set; }
        public virtual ICollection<CustomerOrder> CustomerOrder { get; set; }
    }
}
