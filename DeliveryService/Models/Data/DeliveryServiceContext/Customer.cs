﻿using System;
using System.Collections.Generic;

namespace DeliveryService.Models.Data.DeliveryServiceContext
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerOrder = new HashSet<CustomerOrder>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }
        public string Password { get; set; }

        public virtual ICollection<CustomerOrder> CustomerOrder { get; set; }
    }
}
